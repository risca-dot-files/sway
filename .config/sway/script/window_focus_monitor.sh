#!/usr/bin/env bash
set -eu
set -o pipefail

NAME="sway-window_focus_monitor"
PID=$$

_log() {
  local level=$1; shift
  local msg=$@
  logger -t "${NAME}" -p ${level} ${msg}
}

_log_stdout() {
  local level=$1; shift
  local msg=$@
  echo "[${NAME}]:[${level}] ${msg}"
}

if command -v logger >/dev/null ; then
  HAS_LOGGER=1
else
  echo "Missing utility logger, logs will go to stdout"
  HAS_LOGGER=0
fi

log() {
  if [ "${HAS_LOGGER}" -eq 0 ]; then
    _log $@
  else
    _log_stdout $@
  fi
}

log_info() {
  local msg=$@
  log INFO ${msg}
}
log_error() {
  local msg=$@
  log ERROR ${msg}
}

prev_focus=$(swaymsg -r -t get_seats | jq '.[0].focus')

PIDFILE=/var/run/user/$(id -u)/${NAME}.pid

if [ -f $PIDFILE ]
then
  OLDPID=$(cat $PIDFILE)
  if ps -p $OLDPID > /dev/null 2>&1
  then
    log_info "Process already running"
    kill -s 9 $OLDPID
  fi
  sleep 1
fi

if ( set -o noclobber; echo "$$" > "$PIDFILE") 2> /dev/null;
then
  trap "{ rm $PIDFILE; }" INT TERM EXIT
else
  log_error "Failed to acquire lockfile: $lockfile."
  exit 1
fi

log_info "starting loop"

swaymsg -m -t subscribe '["window"]' | \
  jq --unbuffered 'select(.change == "focus").container.id' | \
  while read new_focus; do
    swaymsg "[con_id=${prev_focus}] mark --add _prev" &>/dev/null
    prev_focus=$new_focus
  done
