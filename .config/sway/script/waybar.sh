#!/usr/bin/env bash
set -eu -o pipefail

LOCK=/run/lock/sway-waybar
PID=$$

#while [ -f "${LOCK}" ]; do
if [ -f "${LOCK}" ]; then
  OLDPID="$(cat "${LOCK}")"
  if [[ "$(ps -p $OLDPID -o command --no-headers)" == *"waybar.sh"* ]]; then
    kill -s 15 $OLDPID
  else
    logger "PID ${OLDPID} not found"
  fi
else
  OLDPID=""
fi

exec 200<>${LOCK} || { logger "Error to write to lock file: ${LOCK}"; exit 1; }
if flock -n 200 ; then
  trap "flock -u 200" EXIT SIGINT SIGKILL SIGTERM
  echo ${PID} >&200
else
  for waittime in $(seq 1 10); do
    if flock -w ${waittime} 200 ; then
      echo ${PID} >&200
      trap "flock -u -n 200" EXIT SIGINT SIGKILL SIGTERM
      break
    else
      if [[ "$(ps -p $OLDPID -o command --no-headers)" == *"waybar.sh"* ]]; then
        kill -s 9 $OLDPID
      else
        logger "PID ${OLDPID} not found"
      fi
    fi
  done
fi

[ -f ~/.config/sway/waybar/$(hostname)/config ] && \
  config_path=~/.config/sway/waybar/$(hostname)/config || \
      config_path=~/.config/sway/waybar/default/config

[ -f ~/.config/sway/waybar/$(hostname)/style.css ] && \
  style_path=~/.config/sway/waybar/$(hostname)/style.css || \
      style_path=~/.config/sway/waybar/default/style.css

# Launch main waybar
waybar -c "${config_path}" -s "${style_path}" &
waybarrunner=$!
wait "${waybarrunner}"
