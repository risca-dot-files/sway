#!/usr/bin/env python3
import re
from pathlib import Path
from typing import List

SWAY_FOLDER = Path.home() / ".config/sway"
SWAY_CONFIG = SWAY_FOLDER / "config"


REINCLUDE = re.compile(r'[ \t]*include[ \t]+(?P<dest>.*)')
REEXEC = re.compile(
        r'[ \t]*exec(_[a-z]+)? +(?P<program>[^#]+)( +#(?P<comment>.+))?')
REBINDGR = re.compile(r'[ \t]*bindsym +\{')
REBINDGRCM = re.compile(
        r'[ \t]*(?P<shortcut>[^#^ ^{^}]+) +(?P<command>[^#].*)( *#(P<comment>.*))?')
REBIND = re.compile(
        r'[ \t]*bindsym +(?P<shortcut>[^ ^{]+) +(?P<command>[^#].*)( *#(P<comment>.*))?')


class ConfigFile:
    """Read and expose configuration of SWAY"""

    def __init__(self, path: Path):
        self.path = path
        self._options = {
                "includes": [],
                "execs": [],
                "binds": []}
        self._parsed = False

    def _add_include(self, destination: str):
        self._options['includes'].append(destination)

    def _add_exec(self, meta: dict):
        self._options['execs'].append(meta)

    def _add_bind(self, meta: dict):
        self._options['binds'].append(meta)

    def parse(self):
        """Read and collect configuration"""
        if self._parsed is True:
            return
        print("Processing ", self.path)
        with self.path.open(mode='r') as f_:
            while line := f_.readline():
                if include := REINCLUDE.match(line):
                    self._add_include(include.groupdict()["dest"])
                    continue
                if exec_ := REEXEC.match(line):
                    self._add_exec(exec_.groupdict())
                if REBINDGR.match(line):
                    while nextline := f_.readline():
                        if re.match(r'[ \t]*}', nextline):
                            break
                        if bindgr := REBINDGRCM.match(nextline):
                            self._add_bind(bindgr.groupdict())
                if bind := REBIND.match(line):
                    self._add_bind(bind.groupdict())
            print('EOF!')
        self._parsed = True

    @property
    def includes(self) -> List[Path]:
        self.parse()
        return [Path(path).expanduser() for path in self._options['includes']]

    @property
    def execs(self):
        self.parse()
        return self._options['execs']

    @property
    def binds(self):
        self.parse()
        return self._options['binds']


if __name__ == '__main__':
    CONF = ConfigFile(SWAY_CONFIG)
    INCLUDES = CONF.includes
    EXECS = CONF.execs
    BINDS = CONF.binds
    for C in CONF.includes:
        if not C.exists():
            print(f'Not found {C}')
            continue
        _ = ConfigFile(C)
        INCLUDES.extend(_.includes)
        EXECS.extend(_.execs)
        BINDS.extend(_.binds)
    print(INCLUDES)
    print(EXECS)
    print(BINDS)
