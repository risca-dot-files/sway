# Variables
set $lock ~/.local/bin/scripts/lock.sh
set $power ~/.config/rofi/modi/power
set $wifi ~/.config/rofi/modi/nmcli
set $screenshot grim ~/Images/screenshots/scrn-$(date +"%Y-%m-%d-%H-%M-%S").png
set $screenclip slurp | grim -g - ~/Images/screenshots/scrn-$(date +"%Y-%m-%d-%H-%M-%S").png
set $background ~/.images/mountainsea.jpg

set $cl_high #009ddc
set $cl_indi #d9d8d8
set $cl_back #231f20
set $cl_fore #d9d8d8
set $cl_urge #ee2e24

# Colors                border   bg       text     indi     childborder
client.focused          $cl_high $cl_high $cl_fore $cl_indi $cl_high
client.focused_inactive $cl_back $cl_back $cl_fore $cl_back $cl_back
client.unfocused        $cl_back $cl_back $cl_fore $cl_back $cl_back
client.urgent           $cl_urge $cl_urge $cl_fore $cl_urge $cl_urge


# Font
font pango:Sans 12

# Window borders
default_border pixel 1
default_floating_border normal
hide_edge_borders smart

smart_gaps on
gaps inner 10

# Autostart
exec --no-startup-id mako
exec --no-startup-id redshift -c ~/.config/redshift/config
exec mailspring
exec spotify
exec riot
exec --no-startup-id nextcloud
exec --no-startup-id libinput-gestures

# Input configuration
input * {
    xkb_layout de
    xkb_variant nodeadkeys
    xkb_options caps:swapescape
}

# Output configuration
output * bg $background fill

# Default workspaces for common programs
assign [class="^Chromium$"] $ws1
# assign [class="^code-oss$"] $ws2
assign [class="^Signal$"] $wsF1
assign [class="^Riot$"] $wsF1
assign [class="^Mailspring$"] $wsF2
assign [app_id="virt-manager"] $wsF8
assign [class="^KeePass2$"] $wsF11
assign [class="^Spotify$"] $wsF12

# Shortcuts
bindsym $mod+Print exec $screenshot
bindsym $mod+Shift+Print exec $screenclip

bindsym $mod+l exec $lock
bindsym $mod+Escape exec $power
bindsym $mod+End exec $wifi

# Notifications
bindsym Control+Space exec makoctl dismiss
bindsym Control+Shift+Space exec makoctl dismiss --all

# Multimedia
bindsym --locked XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume $(pacmd list-sinks |awk '/* index:/{print $3}') +5%
bindsym --locked XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume $(pacmd list-sinks |awk '/* index:/{print $3}') -5%
bindsym --locked XF86AudioMute exec --no-startup-id pactl set-sink-mute $(pacmd list-sinks |awk '/* index:/{print $3}') toggle
bindsym --locked XF86AudioPlay exec playerctl play-pause
bindsym --locked XF86AudioNext exec playerctl next
bindsym --locked XF86AudioPrev exec playerctl previous

# Brightness controls
bindsym --locked XF86MonBrightnessUp exec --no-startup-id light -A 10
bindsym --locked XF86MonBrightnessDown exec --no-startup-id light -U 10

# Toggle Redshift
bindsym $mod+Home exec --no-startup-id pkill -USR1 redshift

# Idle configuration
exec swayidle \
    timeout 300 'exec $lock' \
    timeout 600 'swaymsg "output * dpms off"' \
    after-resume 'swaymsg "output * dpms on"' \
    before-sleep 'exec $lock'




# Layout stuff:


# Modes

set $mode_system System: (l) lock, (e) logout, (s) suspend, (r) reboot, (S) shutdown, (R) UEFI
mode "$mode_system" {
    bindsym l exec $lock, mode "default"
    bindsym e exit
    bindsym s exec --no-startup-id systemctl suspend, mode "default"
    bindsym r exec --no-startup-id systemctl reboot, mode "default"
    bindsym Shift+s exec --no-startup-id systemctl poweroff -i, mode "default"
    bindsym Shift+r exec --no-startup-id systemctl reboot --firmware-setup, mode "default"

    # return to default mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+Shift+e mode "$mode_system"

exec_always ~/.config/waybar/waybar.sh
